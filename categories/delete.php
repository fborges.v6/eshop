<?php
require_once '../shared/verify_session.php';
$title = 'Delete category';
require_once '../shared/header.php';
require_once '../shared/db.php';
require_once '../shared/verify_user.php';

$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);

$categories = $category_model->find_category($id);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $category_model->delete($id);
    return header('Location: /categories');
}
?>
<div class="container">
  <h1><?=$title?></h1>
  <?php
	if ($categories) {
	  foreach ($categories as $category) {
	    echo '<div class="alert alert-danger" role="alert">';
	    echo '<p>'.'Are you sure to delete the '.$category['name'] .' category?'.'</p>';
	    echo '</div>';
	  }
	}
  ?>
  <form method="POST">
    <input class="btn btn-primary" type="submit" value="Acept">
    <a class="btn btn-default btn-danger" href="/categories">Cancel</a>
  </form>
</div>
