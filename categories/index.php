<?php
require_once '../shared/verify_session.php';
$title = 'Categories';
require_once '../shared/header.php';
require_once '../shared/db.php';
require_once '../shared/verify_user.php';
$categories = $category_model->select();
$products = $product_model->select();
?>

<div class="container " style="width: 50%">
  <h1><?=$title?></h1>
   <div class="alert alert-warning" role="alert">Only the categories without associated products will be deleted!</div>
  <br>
  <table class="table table-striped table-bordered">
    <tr>
      <th>Category Name</th>
      <th class="text-center"><a href="/categories/create.php" class="btn btn-success"><i class="fas fa-plus-square fa-2x"></i></a></th>
    </tr>
<?php
if ($categories) {
  foreach ($categories as $category) {
    echo '<tr>';
    echo '<td>' . $category['name'] . '</td>';
    echo '<td>';
    echo '<a href="/categories/update.php?id=' . $category['id'] . '" class="btn mr-2"><i class="fas fa-edit fa-2x"></i></a>';
    $pe=$product_model->exist_category($category['id']);
    if (!$pe and $pe==0) {
        echo '<a href="/categories/delete.php?id=' . $category['id'] . '" class="btn "><i class="far fa-trash-alt fa-2x"></i></a>';
    }
    echo '</td>';
    echo '</tr>';
  }
  
}

?>
  </table>
</div>
