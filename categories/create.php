<?php
require_once '../shared/verify_session.php';
$title = 'Create Category';
require_once '../shared/header.php';
require_once '../shared/verify_user.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require_once '../shared/db.php';
    $category_name = filter_input(INPUT_POST, 'category_name', FILTER_SANITIZE_STRING);
    $category_model->insert($category_name);
    return header('Location: /categories');
}
?>
<div class="container">
  <h1><?=$title?></h1>
  <form method="POST">
    <div class="form-group">
      <label for="category_name">Category Name</label>
      <input type="text" class="form-control"  name="category_name">
    </div>
    <input class="btn btn-primary" type="submit" value="Acept">
    <a class="btn btn-default btn-danger" href="/categories">Cancel</a>
  </form>
</div>
