<?php

require_once __DIR__ . '/../Db/PgConnection.php';
require_once __DIR__ . '/../Models/User.php';
require_once __DIR__ . '/../Models/Category.php';
require_once __DIR__ . '/../Models/Product.php';
require_once __DIR__ . '/../Models/Cart.php';
require_once __DIR__ . '/../Models/Items.php';

use Db\PgConnection;
$con = new PgConnection('postgres', 'vik', 'eshops', 5432, 'localhost');
$con->connect();

$user_model = new Models\User($con);
$category_model = new Models\Category($con);
$product_model = new Models\Product($con);
$cart_model = new Models\Cart($con);
$items_model = new Models\Items($con);
