<?php
require_once __DIR__ . '/sessions.php';
require_once __DIR__ . '/db.php';
if (!isset($_SESSION['user_id']) || empty($_SESSION['user_id'])) {
    return header('Location: /security/login.php');
}
$logged_in = $user_model->find($_SESSION['user_id']);
