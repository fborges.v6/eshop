<?php
  require_once __DIR__ . '/sessions.php';
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">E-Shop</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
        <?php
        $menu = [
          'Home' => '/',
          'Categories' => '../categories/index.php',
          'Products' => '../products/index.php',       
        ];

        if (isset($_SESSION['user_id']) || !empty($_SESSION['user_id'])) {
            foreach ($menu as $key => $value) {
                echo "<li class='nav-item'>
                      <a class='nav-link' href='$value'>$key</a>
                    </li>";
            }
        }
        ?>
    </ul>
    <?php
      if (isset($_SESSION['user_id']) || !empty($_SESSION['user_id'])) {
    ?>
    <ul class="nav navbar-nav navbar-right">
      <li><a href='/security/logout.php'><i class="fas fa-sign-out-alt fa-2x"></i></a></li>
    </ul>
    <?php
    }
    ?>
  </div>
</nav>
<?php
require_once '../shared/footer.php';
?>