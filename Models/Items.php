<?php
namespace Models{

	/**
	* 
	*/
	class Items
	{
		private $connection;		
		function __construct($connection)
		{
			$this->connection = $connection;
		}
		public function insert($id_item,$id_cart)
        {
            $this->connection->runStatement('INSERT INTO cart_items(id_item, id_cart ) VALUES ($1,$2)', [$id_item,$id_cart]);
        }
	}
}
?>