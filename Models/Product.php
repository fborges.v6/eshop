<?php

namespace Models {
    class Product
    {
        private $connection;
        public function __construct($connection)
        {
            $this->connection = $connection;
        }

        public function update_basic($id,$sku,$name,$description,$stock,$price,$category)
        {
            $this->connection->runStatement('UPDATE products SET sku = $2, name = $3, description = $4, stock = $5, price = $6, category = $7  WHERE id = $1', [$id,$sku,$name,$description,$stock,$price,$category]);
        }
        public function update($id,$sku,$name,$description,$stock,$price,$category,$img)
        {
            $this->connection->runStatement('UPDATE products SET sku = $2, name = $3, description = $4, stock = $5, price = $6, category = $7,image = $8  WHERE id = $1', [$id,$sku,$name,$description,$stock,$price,$category,$img]);
        }

        public function find($id)
        {
            return $this->connection->runQuery('SELECT * FROM products WHERE id = $1', [$id])[0];
        }
         public function find_img($id)
        {
            return $this->connection->runQuery('SELECT image FROM products WHERE id=$1',[$id]);
        }
        public function select()
        {
            return $this->connection->runQuery('SELECT * FROM products ORDER BY id');
        }
         public function exist_category($id)
        {
            return $this->connection->runQuery('SELECT * FROM products WHERE category=$1',[$id]);
        }
        public function find_id($id)
        {
            return $this->connection->runQuery('SELECT * FROM products WHERE id=$1',[$id]);
        }
        public function find_item($id)
        {
            return $this->connection->runQuery('SELECT * FROM products WHERE category=$1',[$id]);
        }
       public function delete($id)
        {
            $this->connection->runStatement('DELETE FROM products WHERE id = $1', [$id]);
        }
        public function insert($sku,$name,$description,$stock,$price,$category,$img)
        {
            $this->connection->runStatement('INSERT INTO products(sku, name, description, stock, price, category,image) VALUES ($1,$2,$3,$4,$5,$6,$7)', [$sku,$name,$description,$stock,$price,$category,$img]);
        }

    }
}
