<?php

namespace Models {
    class Cart
    {
        private $connection;
        public function __construct($connection)
        {
            $this->connection = $connection;
        }

        public function insert($id,$name)
        {
            $this->connection->runStatement('INSERT INTO carts(user_id, name ) VALUES ($1,$2)', [$id,$name]);
        }

        public function find($id)
          {
             $result = $this->connection->runQuery('SELECT * FROM carts WHERE user_id=$1', [$id]);
             return $result;
          }
    }
}
