<?php

namespace Models {
    class User
    {
        private $connection;
        public function __construct($connection)
        {
            $this->connection = $connection;
        }
         public function select()
        {
            return $this->connection->runQuery('SELECT COUNT (role) FROM users WHERE role="customer"');
        }
        public function login($username, $password)
        {
          $result = $this->connection->runQuery('SELECT * FROM users WHERE username = $1 and password = md5($2)', [$username, $password]);
          return $result[0];
        }

        public function insert($name,$lastname,$phone,$email,$address,$username,$password)
        {
            $sql = "INSERT INTO users(name,lastname,phone,email,address,username,password,role) VALUES ($1,$2,$3,$4,$5,$6, md5($7),'customer')";
            $this->connection->runStatement($sql, [$name,$lastname,$phone,$email,$address,$username,$password]);
        }
        public function findLast()
          {
             $result = $this->connection->runQuery('SELECT * FROM users ORDER BY id DESC LIMIT 1');
             return $result;
          }

        public function find($id)
          {
             $result = $this->connection->runQuery('SELECT * FROM users WHERE id=$1', [$id]);
             return $result;
          }
        


    }
}
