<?php

namespace Models {
    class Category
    {
        private $connection;
        public function __construct($connection)
        {
            $this->connection = $connection;
        }

        public function find($id)
        {
            return $this->connection->runQuery('SELECT * FROM categories WHERE id = $1', [$id])[0];
        }
        public function find_category($id)
        {
            return $this->connection->runQuery('SELECT * FROM categories WHERE id = $1', [$id]);
        }

        public function select()
        {
            return $this->connection->runQuery('SELECT * FROM categories ORDER BY id');
        }
        public function select_next($id)
        {
            return $this->connection->runQuery('SELECT * FROM categories WHERE id != $1', [$id]);
        }

        public function insert($name)
        {
            $this->connection->runStatement('INSERT INTO categories(name) VALUES ($1)', [$name]);
        }

       public function update($id, $name)
        {
            $this->connection->runStatement('UPDATE categories SET name = $2 WHERE id = $1', [$id, $name]);
        }

        public function delete($id)
        {
            $this->connection->runStatement('DELETE FROM categories WHERE id = $1', [$id]);
        }
    }
}
