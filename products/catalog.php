<?php
require_once '../shared/verify_session.php';
$title = 'Categories';
require_once '../shared/header.php';
require_once '../shared/db.php';
require_once '../shared/verify_user.php';
$categories = $category_model->select();
$products = $product_model->select();
?>

<div class="container " style="width: 50%">
  <h1><?=$title?></h1>
  <br>
  <table class="table table-striped table-bordered">
    <tr>
      <th>Category Name</th>
      <th class="text-center"></th>
    </tr>
<?php
if ($categories) {
  foreach ($categories as $category) {
    echo '<tr>';
    echo '<td>' . $category['name'] . '</td>';
    echo '<td>';
    echo '<a href="view.php?id=' . $category['id'] . '" class="btn mr-2"><i class="far fa-eye fa-2x"></i></a>';
    echo '</td>';
    echo '</tr>';
  }
  
}

?>
  </table>
</div>
