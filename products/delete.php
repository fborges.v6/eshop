<?php
require_once '../shared/verify_session.php';
$title = 'Delete products';
require_once '../shared/header.php';
require_once '../shared/db.php';
require_once '../shared/verify_user.php';

$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
$products=$product_model->find_id($id);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$img_name=$product_model->find_img($id);
	foreach ($img_name as $img) {
		$data=unlink("../assets/img/".$img['image']);
	}
    $product_model->delete($id); 
    return header('Location: /products');
}
?>
<div class="container">
  <h1><?=$title?></h1>
  <?php
	if ($products and $products>0) {
	  foreach ($products as $product) {
	    echo '<div class="alert alert-danger" role="alert">';
	    echo '<p>'.'Are you sure to delete the '.$product['name'] .'?'.'</p>';
	    echo '</div>';
	  }
	}
  ?>
  <form method="POST">
    <input class="btn btn-primary" type="submit" value="Acept">
    <a class="btn btn-default btn-danger" href="/products">Cancel</a>
  </form>
</div>
