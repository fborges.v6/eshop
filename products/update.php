<?php
require_once '../shared/verify_session.php';
$title = 'Update item';
require_once '../shared/header.php';
require_once '../shared/verify_user.php';
require_once '../shared/db.php';

$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
$products = $product_model->find($id);

if($_SERVER['REQUEST_METHOD'] == 'POST'){

  $sku = filter_input(INPUT_POST, 'sku', FILTER_SANITIZE_STRING);
  $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
  $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
  $stock = filter_input(INPUT_POST, 'stock', FILTER_SANITIZE_STRING);
  $price = filter_input(INPUT_POST, 'price', FILTER_SANITIZE_STRING);
  $category = filter_input(INPUT_POST, 'category_list', FILTER_SANITIZE_STRING);

  if ($_FILES['file']['name']=="") {
    $product_model->update_basic($id,$sku,$name,$description,$stock,$price,$category);
  }
  else{
    $info=$product_model->find_img($id);
    foreach ($info as $data) {
      $reset=unlink("../assets/img/".$data['image']);
    }
    $img=$_FILES['file']['name'];
    $targed_dir="../assets/img/";
    $targed_file=$targed_dir .basename($_FILES['file']['name']);
    move_uploaded_file($_FILES['file']['tmp_name'],$targed_dir.$img);
    $product_model->update($id,$sku,$name,$description,$stock,$price,$category,$img);
  }
  return header('Location: /products');
}
?>
<!DOCTYPE html>
<html>
  <body>
    <div class="container">
      <h1><?=$title?></h1>
      <div class="row">
        <div class="col-md-6">
      		<div>
      			<form method="POST" enctype="multipart/form-data">
              <div class="form-group">
                <input type="text" class="form-control" name="sku" placeholder="SKU" value="<?=$products['sku']?>" />
              </div>
      				<div class="form-group">
      					<input type="text" class="form-control" name="name" placeholder="Product Name" value="<?=$products['name']?>" />
      				</div>
              <div class="form-group">
      					<input type="number" class="form-control" name="price" min="0" placeholder="Price" value="<?=$products['price']?>" />
      				</div>
              <div class="form-group">
                <input type="number" class="form-control" name="stock" min="0" placeholder="Stock" value="<?=$products['stock']?>" />
              </div>
              <div class="form-group">
                <input class="form-control" name="description" rows="8" cols="80" placeholder="Description" value="<?=$products['description']?>" ></input>
      				</div>
              <div class="form-group">
      					<input type="file" class="form-control" name="file" />
      				</div>
              <div class="form-group">
                <select class="form-control" name="category_list" >
                  <?php
                    $result_array = $category_model->select_next($products['category']);
                    $result_category = $category_model->find($products['category']);
                    if ($result_array>0) {
                      if ($result_category>0) {
                        echo "<option value='{$result_category['id']}'>{$result_category['name']}</option>";                        
                      }
                      foreach ($result_array as $row) {
                        echo "<option value='{$row['id']}'>{$row['name']}</option>";
                      }
                    }
                  ?>
                </select>
      				</div>
      				<div class="form-group last">
                <div class="row">
                  <div class="col-md-2">
                    <input type="submit" class="btn btn-primary" value="Acept">
                  </div>
                  <div class="col-md-2">
                    <a class="btn btn-default btn-danger" href="/products">Cancel</a>
                  </div>
                </div>
      				</div>
      			</form>
      		</div>
        </div>
      </div>
    </div>
  <?php
  require_once '../shared/footer.php';
  ?>
  </body>
</html>
