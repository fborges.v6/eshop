<?php
require_once '../shared/verify_session.php';
$title = 'Products';
require_once '../shared/header.php';
require_once '../shared/db.php';
require_once '../shared/verify_user.php';
$products = $product_model->select();
?>

<div class="container">
  <nav class="navbar navbar-light">
    <h1>Products</h1>
    <form class="form-inline">
      <a href="/products/new.php" class="btn btn-success"><i class="fas fa-plus-square fa-2x"></i></a>
    </form>
  </nav>
  <br>
  <div class="row">
    <?php
      if ($products) {
        foreach ($products as $product) {
          echo '<div class="col-md-4">';
          echo '<div class="card">';
          echo '<h6 class="card-header">'.$product['name'].'</h6>';
          echo '<img src="../assets/img/'.$product['image'].'" class="card-img-top" alt="Card image cap" style="width:100%">';
          echo '<div class="card-body">';
          echo '<h5 class="card-title">'.'$'.$product['price'].'</h5>';
          echo '<p class="card-text">'.$product['description'].'</p>';
          echo '</div>';
          echo '<div class="card-footer">';
          echo '<a href="/products/update.php?id=' . $product['id'] . '" class="btn btn-sm mr-2"><i class="fas fa-edit fa-2x"></i></a>';
          echo '<a href="/products/delete.php?id=' . $product['id'] . '" class="btn btn-sm"><i class="far fa-trash-alt fa-2x"></i></a>';
          echo '</div>';
          echo '</div>';
          echo '<br>';
          echo '</div>';
        }
      } 
    ?>
  </div>
 </div>


<div class=""></div>