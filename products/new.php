<?php
require_once '../shared/verify_session.php';
$title = 'New item';
require_once '../shared/header.php';
require_once '../shared/verify_user.php';
require_once '../shared/db.php';

if($_SERVER['REQUEST_METHOD'] == 'POST'){
  $img=$_FILES['file']['name'];
  $targed_dir="../assets/img/";
  $targed_file=$targed_dir .basename($_FILES['file']['name']);

  $sku = filter_input(INPUT_POST, 'sku', FILTER_SANITIZE_STRING);
  $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
  $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
  $stock = filter_input(INPUT_POST, 'stock', FILTER_SANITIZE_STRING);
  $price = filter_input(INPUT_POST, 'price', FILTER_SANITIZE_STRING);
  $category = filter_input(INPUT_POST, 'category_list', FILTER_SANITIZE_STRING);

  $product_model->insert($sku,$name,$description,$stock,$price,$category,$img);

  move_uploaded_file($_FILES['file']['tmp_name'],$targed_dir.$img);
  return header('Location: /products');
}
?>
<!DOCTYPE html>
<html>
  <body>
    <div class="container">
      <h1><?=$title?></h1>
      <div class="row">
        <div class="col-md-6">
      		<div>
      			<form method="POST" enctype="multipart/form-data">
              <div class="form-group">
                <input type="text" class="form-control" name="sku" placeholder="SKU" required />
              </div>
      				<div class="form-group">
      					<input type="text" class="form-control" name="name" placeholder="Product Name" required />
      				</div>
              <div class="form-group">
      					<input type="number" class="form-control" name="price" min="0" placeholder="Price" required/>
      				</div>
              <div class="form-group">
                <input type="number" class="form-control" name="stock" min="0" placeholder="Stock" required/>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="description" rows="8" cols="80" placeholder="Description" required></textarea>
      				</div>
              <div class="form-group">
      					<input type="file" class="form-control" name="file" required />
      				</div>
              <div class="form-group">
                <select class="form-control" name="category_list" required>
                  <?php
                    $result_array = $category_model->select();
                    if ($result_array>0) {
                      foreach ($result_array as $row) {
                        echo "<option value='{$row['id']}'>{$row['name']}</option>";
                      }
                    }
                  ?>
                </select>
      				</div>
      				<div class="form-group last">
                <div class="row">
                  <div class="col-md-2">
                    <input type="submit" class="btn btn-primary" value="Acept">
                  </div>
                  <div class="col-md-2">
                    <a class="btn btn-default btn-danger" href="/products">Cancel</a>
                  </div>
                </div>
      				</div>
      			</form>
      		</div>
        </div>
      </div>
    </div>
  <?php
  require_once '../shared/footer.php';
  ?>
  </body>
</html>
