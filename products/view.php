<?php
require_once '../shared/verify_session.php';
$title = 'Items';
require_once '../shared/header.php';
require_once '../shared/db.php';
require_once '../shared/verify_user.php';

$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
$products = $product_model->find_item($id);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $item_id = $_POST['add'];
  if (isset($logged_in)) {
    foreach ($logged_in as $value) {
      $result = $cart_model->find($value['id']);
    }
  }
  $data = $cart_model->find($value['id']);
  foreach ($data as $row) {
    $items_model->insert($item_id,$row['id']);
  }
  //return header('Location: /products/catalog.php'); 
}

?>

<div class="container">
  <nav class="navbar navbar-light">
    <h1>Products</h1>
  </nav>
  <br>
  <div class="row">
    <?php
      if ($products) {
        foreach ($products as $product) {
          echo '<div class="col-md-4">';
          echo '<div class="card">';
          echo '<h5 class="card-header">'.$product['name'].'</h5>';
          echo '<img src="../assets/img/'.$product['image'].'" class="card-img-top" alt="Card image cap" style="width:100%">';
          echo '<div class="card-body">';
          echo '<h5 class="card-title">'.'$'.$product['price'].'</h5>';
          echo '<p class="card-text">'.'SKU: '.$product['sku'].'</p>';
          echo '<p class="card-text">'.$product['description'].'</p>';
          echo '</div>';
          echo '<div class="card-footer">';
          echo '<form method="post">';
          echo '<button name="add" value='.$product['id'].' class="btn btn-success btn-sm mr-2">Add</button>';
          echo '</form>';
          echo '</div>';
          echo '</div>';
          echo '<br>';
          echo '</div>';
        }
      } 
    ?>
  </div>
 </div>
