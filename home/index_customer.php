<?php
$title = 'Home';
require_once '../shared/header.php';
require_once '../shared/sessions.php';

if (!isset($_SESSION['user_id']) || empty($_SESSION['user_id'])) {
    return header('Location: /security/login.php');
}
else {
	require_once '../shared/verify_user.php';
}
?>

<div class="container" id="main_view_admin">
	<div class="row">
		<div class="col-md-12">
			<div class="card-deck">
			  <div class="card">
			    <i class="fas fa-shopping-bag fa-10x"></i>
			    <div class="card-body">
			      <h5 class="card-title">Total of Products Purchased </h5>
			      <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
			     
			    </div>
			  </div>
			  <div class="card">
			    <i class="fas fa-hand-holding-usd fa-10x"></i>
			    <div class="card-body">
			      <h5 class="card-title">Total Amount of Purchases</h5>
			      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
			      
			    </div>
			  </div>
			  <div class="card">
			  	<a href="../products/catalog.php"><i class="far fa-calendar-alt fa-10x"></i></a>
			    <div class="card-body">
			      <h5 class="card-title">Product Catalog</h5>
			      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
			      
			    </div>
			  </div>
			</div>
		</div> 
	</div> 
</div> 
