<?php
$title = 'Login';
require_once '../shared/header.php';
require_once '../shared/db.php';
require_once '../shared/sessions.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
    $user = $user_model->login($username, $password);
    if ($user != null) {
          $_SESSION['user_id'] = $user['id'];
          $_SESSION['user_role'] = $user['role'];
          if ($_SESSION['user_role']=='administrator') {
            return header('Location: /home/index.php');
          }
          else {
            return header('Location: /home/index_customer.php');
          }
          
    } else {
          echo "<h3>Invalid Username or Password!</h3>";
    }
}
?>
<div class=" principal container">
  <h1><?=$title?></h1>
  <div class="row">
    <div class="col-md-12">
      <form method="POST">
        <div class="form-group">
          <label for="username"></label>
          <input type="text" class="form-control" placeholder="Username" name="username">
        </div>
        <div class="form-group">
          <input type="password" class="form-control" placeholder="Password" name="password">
        </div>
        <input class="btn btn-primary" type="submit" value="Login!">
        <a class="btn btn-default" href="/security/signup.php">Signup</a>
      </form>
    </div>
  </div>
</div>
