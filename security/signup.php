<?php
$title = 'Register';
require_once '../shared/header.php';
require_once '../shared/db.php';

$name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
$lastname = filter_input(INPUT_POST, 'lastname', FILTER_SANITIZE_STRING);
$phone_number = filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_STRING);
$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
$address = filter_input(INPUT_POST, 'address', FILTER_SANITIZE_STRING);
$username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
$password_verification = filter_input(INPUT_POST, 'password_verification', FILTER_SANITIZE_STRING);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($password == $password_verification) {
        $user_model->insert($name,$lastname,$phone_number,$email,$address,$username,$password);
        $result =$user_model->findLast();
        foreach ($result as $row) {
          $cart_model->insert($row['id'], $row['name']);
        }
        return header("Location:/security/login.php");       
    }
    echo "<h3>Your password and confirmation password do not match.</h3>";
}
?>
<div class="container">
  <h1><?=$title?></h1>
  <div class="row">
    <div class="col-md-6">
      <form method="POST">
        <div class="form-group">
          <label for="name">Name</label>
          <input type="text" class="form-control" name="name">
        </div>
        <div class="form-group">
          <label for="lastname">Lastname</label>
          <input type="text" class="form-control" name="lastname">
        </div>
        <div class="form-group">
          <label for="phone">Phone number</label>
          <input type="text" class="form-control"  name="phone">
        </div>
        <div class="form-group">
          <label for="email">Email address</label>
          <input type="email" class="form-control" aria-describedby="emailHelp" name="email">
        </div>
        <div class="form-group">
         <label for="address">Address</label>
         <input type="text" class="form-control" name="address">
        </div>
        <div class="form-group">
         <label for="username">Username</label>
         <input type="text" class="form-control"  name="username">
        </div>
        <div class="form-group">
          <label for="password">Password</label>
          <input type="password" class="form-control" name="password">
        </div>
        <div class="form-group">
          <label for="password_verification">Re-enter password</label>
          <input type="password" class="form-control" id="password_verification" name="password_verification">
        </div>
        <input class="btn btn-primary" type="submit" value="Login!">
        <a class="btn btn-default" href="/security/signup.php">Signup</a>
      </form>
    </div>
  </div>
</div>
